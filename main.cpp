#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

const int MatrixSize = 9;

int ** InitializeMatrix(void);
void SetMatrixRandom(int **, int, int);
void PrintMatrix(int **);
int FindNumberPositiveElementsSecondaryDiagonal(int **);
void ReplaceAllNegativeElementsUnderMainDiagonal(int **, int);

int main()
{
	srand(time(0));
	int **Matrix = InitializeMatrix();
	SetMatrixRandom(Matrix,-21,23);
	PrintMatrix(Matrix);
	cout << "The number of positive elements on the main diagonal = " 
		 << FindNumberPositiveElementsSecondaryDiagonal(Matrix) << endl << endl;
	ReplaceAllNegativeElementsUnderMainDiagonal(Matrix,0);
	PrintMatrix(Matrix);
	return 0;
}

int ** InitializeMatrix(void)
{	
	int **matrix = new int *[MatrixSize];
	for (int i = 0;i<MatrixSize+1;i++)
	{
		matrix[i] = new int[MatrixSize];
	}
	return matrix;
}

void SetMatrixRandom(int **matrix, int left, int right)
{
	for (int i=0;i<MatrixSize;i++)
	{
		for (int j=0;j<MatrixSize;j++)
		{
			matrix[i][j] = rand()%(right-left+1) + left;
		}
	}
}

void PrintMatrix(int **matrix)
{
	for (int i=0;i<MatrixSize;i++)
	{
		for (int j=0;j<MatrixSize;j++)
		{
			cout.width(4);
			cout << matrix[i][j];
		}
		cout << endl << endl;
	}
	cout << endl;
}

int FindNumberPositiveElementsSecondaryDiagonal(int **matrix)
{
	int count = 0;
	for (int i=0;i<MatrixSize;i++)
	{
		for (int j=0;j<MatrixSize;j++)
		{
	        if(i+j == MatrixSize and (matrix[i][j] > 0) )
	        {
	        	count++;
			}
	    }
	}
	return count;
}

void ReplaceAllNegativeElementsUnderMainDiagonal(int **matrix, int element)
{
	for(int i=0;i<MatrixSize;i++)
	{
		for (int j=0;j<MatrixSize;j++)
		{
			if (i>j and matrix[i][j] < 0)
			{
				matrix[i][j] = element;
			}
		}
	}
}